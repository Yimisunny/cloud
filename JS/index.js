/**
 * 模拟图片数据
 */
var imgUrl = [
  "IMG/1.PNG",
  "IMG/2.PNG",
  "IMG/3.PNG",
  "IMG/4.PNG",
  "IMG/5.PNG",
  "IMG/6.PNG",
  "IMG/7.PNG",
  "IMG/8.PNG",
  "IMG/9.PNG",
  "IMG/10.PNG",
  "IMG/11.PNG",
  "IMG/12.PNG",
  "IMG/13.PNG"
];

$(function() {
  /**
   * 点击车系图片切换
   */
  var selectSpan = $(".select-span span");
  for (var i = 0, len = selectSpan.length; i < len; i++) {
    (function(i) {
      selectSpan[i].onclick = function() {
        $("#left-top-img").attr("src", imgUrl[i]);
        $(selectSpan).removeClass("active");
        $(this).addClass("active");
      };
    })(i);

    if (i > 10) {
      $(selectSpan[i]).css({ display: "none" });
    }
  }

  /**
   * 更多活动车系
   */

  var moreCar = $("#more-car");
  if (selectSpan.length > 11) {
    $(moreCar).css({ display: "block" });
  }

  $(moreCar).click(function() {
    $(moreCar).css({ display: "none" });
    $(selectSpan).css({ display: "inline-block" });
  });

  /**
   * 询底价
   */
  $("#button-submit").click(function() {
    if ($("#userName").val() && $("#phoneNum").val()) {
      $("#name-tip,#phone-tip").css({ display: "none" });
      alert("姓名: " + $("#userName").val() + "手机号:" + $("#phoneNum").val());
    } else if (!$("#userName").val()) {
      $("#name-tip").css({ display: "block" });
    } else if (!$("#phoneNum").val()) {
      $("#phone-tip").css({ display: "block" });
    }
  });
  $("#userName").change(function() {
    $("#name-tip").css({ display: "none" });
  });
  $("#phoneNum").change(function() {
    $("#phone-tip").css({ display: "none" });
  });

  /**
   * 分享
   */
  $("#wechat").hover(
    function() {
      $(this).attr("src", "IMG/wechat_selected.png");
      $("#qr-code-box").css({ display: "block" });
    },
    function() {
      $(this).attr("src", "IMG/wechat.png");
      $("#qr-code-box").css({ display: "none" });
    }
  );
  $("#blog").hover(
    function() {
      $(this).attr("src", "IMG/blog_selected.png");
    },
    function() {
      $(this).attr("src", "IMG/blog.png");
    }
  );
  $("#qq_zone").hover(
    function() {
      $(this).attr("src", "IMG/qq_zone_selected.png");
    },
    function() {
      $(this).attr("src", "IMG/qq_zone.png");
    }
  );

  /**
   * 锚点
   */
  $("#active-car-introduce").click(function() {
    document.getElementById("new-enter").scrollIntoView();
    $(".nav-content span").removeClass("active");
    $(this).addClass("active");
    $("#active-car-introduce2").addClass("active");
  });
  $("#active-car-type").click(function() {
    document.getElementById("car-type").scrollIntoView();
    $(".nav-content span").removeClass("active");
    $(this).addClass("active");
    $("#active-car-type2").addClass("active");
  });
  $("#active-car-dealer").click(function() {
    document.getElementById("car-dealer").scrollIntoView();
    $(".nav-content span").removeClass("active");
    $(this).addClass("active");
    $("#active-car-dealer2").addClass("active");
  });

  $("#active-car-introduce2").click(function() {
    document.getElementById("new-enter").scrollIntoView();
    $(".nav-content span").removeClass("active");
    $(this).addClass("active");
    $("#active-car-introduce").addClass("active");
  });
  $("#active-car-type2").click(function() {
    document.getElementById("car-type").scrollIntoView();
    $(".nav-content span").removeClass("active");
    $(this).addClass("active");
    $("#active-car-type").addClass("active");
  });
  $("#active-car-dealer2").click(function() {
    document.getElementById("car-dealer").scrollIntoView();
    $(".nav-content span").removeClass("active");
    $(this).addClass("active");
    $("#active-car-dealer").addClass("active");
  });

  $(window).scroll(function() {
    if ($(window).scrollTop() < 1320) {
      $(".nav-content span").removeClass("active");
      $("#active-car-introduce2").addClass("active");
      $("#active-car-introduce").addClass("active");
    } else if ($(window).scrollTop() > 1320 && $(window).scrollTop() < 2075) {
      $(".nav-content span").removeClass("active");
      $("#active-car-type2").addClass("active");
      $("#active-car-type").addClass("active");
    } else if ($(window).scrollTop() > 2075) {
      $(".nav-content span").removeClass("active");
      $("#active-car-dealer2").addClass("active");
      $("#active-car-dealer").addClass("active");
    }
  });

  /**
   * 滑动
   */
  var mySwiper = new Swiper(".swiper-container", {
    autoplay: true //可选选项，自动滑动
  });

  /**
   * 倒计时
   */
  var leftTime, days, hours, minutes, seconds;

  function leftTimer(year, month, day, hour, minute, second) {
    leftTime =
      new Date(year, month - 1, day, hour, minute, second) - new Date(); //计算剩余的毫秒数

    days = parseInt(leftTime / 1000 / 60 / 60 / 24, 10); //计算剩余的天数
    hours = parseInt((leftTime / 1000 / 60 / 60) % 24, 10); //计算剩余的小时
    minutes = parseInt((leftTime / 1000 / 60) % 60, 10); //计算剩余的分钟
    seconds = parseInt((leftTime / 1000) % 60, 10); //计算剩余的秒数
    days = checkTime(days);
    hours = checkTime(hours);
    minutes = checkTime(minutes);
    seconds = checkTime(seconds);

    $("#day").text(days);
    $("#hour").text(hours);
    $("#minute").text(minutes);
    $("#second").text(seconds);
  }
  clearInterval(timer);
  var timer = setInterval(function() {
    leftTimer(2019, 8, 9, 18, 55, 00);
  }, 1000);
  function checkTime(i) {
    //将0-9的数字前面加上0，例1变为01
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }
  leftTimer(2019, 8, 9, 18, 55, 00);

  /**
   * 滚动显示头部导航
   */
  var mTop = $("#left-tip").offset().top;
  $(document).scroll(function() {
    var scroH = $(document).scrollTop(); //滚动高度
    if (scroH > mTop) {
      //距离顶部大于100px时
      $("#fixed-top-nav-content").css({ display: "block" });
    } else {
      $("#fixed-top-nav-content").css({ display: "none" });
    }
  });

  /**
   * 弹窗询底价
   */

  $("#ask-pop").css({ height: $(document.body).height() });
  $(".ask-btn").click(function() {
    $("#pop-desc-color").text(
      $(this)
        .prev()
        .prev()
        .text()
    );
    $("#ask-pop").css({ display: "block" });
  });

  $("#close-pop").click(function() {
    $("#ask-pop").css({ display: "none" });
    $("#ask-name,#ask-phone").val("");
    $("#ask-name-tip,#ask-phone-tip").css({ display: "none" });
  });

  $("#ask-submit").click(function() {
    if ($("#ask-name").val() && $("#ask-phone").val()) {
      $("#ask-name-tip,#ask-phone-tip").css({ display: "none" });

      alert(
        "姓名: " + $("#ask-name").val() + "手机号:" + $("#ask-phone").val()
      );
      $("#ask-name,#ask-phone").val("");
    } else if (!$("#ask-name").val()) {
      $("#ask-name-tip").css({ display: "block" });
    } else if (!$("#ask-phone").val()) {
      $("#ask-phone-tip").css({ display: "block" });
    }
  });

  $("#ask-name").change(function() {
    $("#ask-name-tip").css({ display: "none" });
  });
  $("#ask-phone").change(function() {
    $("#ask-phone-tip").css({ display: "none" });
  });
});
